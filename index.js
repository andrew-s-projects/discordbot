const Discord = require("discord.js");
const { toNamespacedPath } = require("path");
//const { GuildScheduledEventPrivacyLevels } = require("discord.js/typings/enums");
const config = require("./config.json");
var messagesdb = require("./messagesdb");
const Intents = require("discord.js")
const client = new Discord.Client({intents: ["GUILDS", "GUILD_MESSAGES", "GUILD_MEMBERS", "GUILD_VOICE_STATES", "GUILD_MESSAGE_REACTIONS"]});
client.login(config.BOT_TOKEN);

const prefix = "!"
const axios = require('axios');

const guild = Discord.Guild

// imports from discordjs/voice necessary for audio playing
const { createAudioPlayer, NoSubscriberBehavior } = require('@discordjs/voice');
const { createAudioResource } = require('@discordjs/voice');
const { joinVoiceChannel } = require('@discordjs/voice');
const { channel } = require("diagnostics_channel");
const { AudioPlayerStatus } = require('@discordjs/voice');
const ytdl = require('ytdl-core');

// used to connect with Youtube API
// opts contains parameters for search
const search_u = require('youtube-search'); 
const search = require("youtube-search");
const string_similarity = require("string-similarity");
const lyrics_finder = require('lyrics-finder');

// used to fork twitter python script
const { spawn } = require('child_process');
const { unescape } = require("querystring");
const e = require("express");
const { measureMemory } = require("vm");
const { kill } = require("process");
const { command } = require("commander");
const { test } = require("media-typer");
const { proc } = require("process");

var opts = {
    maxResults: 5,
    key: 'AIzaSyA9ANeVQfb1CoGSKybJA5YUgNl4y9Fz8QQ'
}; 

var command_list = new Map(); 
var search_map = {};
var react_messages ={};
var song_queue = [];
var emojis = [];
const react_num = 2;
const max_channels = 3;
var current_channels = 0;
var monitor_list = new Set();
var active_thread = new Map();


// initialzie command lookup table - used for command validation 
client.on("ready", () => {
    
    command_list.set('ping', [String(null), Number('0'), "Try !ping User #pings"]);
    command_list.set('lyrics',[String(null), "nonuser","Try !lyrics 'name of song' "]);
    command_list.set('play', ["nonuser", "Try !play artist song name"]);
    command_list.set('collect', [String(null), "Try !collect user"]);
    command_list.set('stopc', ["nonuser", " "]);
    command_list.set('show', [String(null), String (null), "Try !show user search term"]);
    command_list.set('tweet', [String(null), "nonuser", "Try !tweet username"]);
    command_list.set('stoptweet', ["nonuser", ""]);
    command_list.set('live', [String(null), "nonuser", "Try !isLive username"]);
});

client.on('messageReactionAdd', reaction =>{

    if(reaction.emoji.name = "joy"){

        let message = reaction.message.content;

        const childPython = spawn('python', ['hash.py', message]);

        childPython.stdout.on('data', (data) =>{
            
            const output = `${data}`;
            const key = react_messages[output];

            console.log(output);
            
            // if message is already in map, update count
            if(key != undefined){

                let key_one = react_messages[output][0];
                react_messages[output][0] = key_one + 1;

                // check to see if tweet
                if(react_messages[output][0] == react_num){

                    let message = react_messages[output][1];
                    const tweet = spawn('python', ['tapp.py', 'tweet', message]);
                    tweet.stdout.on('data', (data) =>{
                        console.log(`${data}`);
                    });
                }
            }
            // otherwise add entry to map
            else{
                react_messages[output] = [1, message];
            }
    });
    
    }
});

// message event handler 
client.on("messageCreate", message =>{


    if(message.author.bot)return;  //ignore message from bot

    if(!message.content.startsWith(prefix)){
    
        // message.channel.send('..collecting messages...');
    }

    //command parser
    const commandBody = message.content.slice(prefix.length);
    const args = commandBody.split(' ');
    const command = args.shift().toLowerCase();

    // implement functions containing users in valUser 
    // will validate user before continuing command

    function valUser(){

        // fetch user map from guild - map members to display name, will also cache users
        const Members = message.guild.members.fetch().then(members =>{
            return members.map(member => member.displayName);
            
        });

        const getMembers = async () => {
            const a = await Members;
            console.log(a);
            
            // check to see if we have a multi-word username
            let multi_word = " ";
            if(a.indexOf(args[0]) == -1){

                let test_str = args[0];
                // test_str += args[0] + " ";
                for (var i = 1; i <= args.length; i++){
                    
                    if(a.indexOf(test_str) != -1){
                        multi_word = test_str;
                        break;
                    }
                    else{
                        test_str += " " + args[i];
                    }
                }
            }
            // check to see if user exists

            if(multi_word != " "){
               var user_index = a.indexOf(multi_word);
               var username = multi_word;
               // new arg list contains multi word username and rest of args of original argv
               var new_argv = [multi_word, args.splice(multi_word.split().length + 2).join(" ")];
            }
            else{
               var user_index = a.indexOf(args[0]);
               var username = args[0];
               var new_argv = args
            }

            //if user exists, continue with command
            if(user_index != -1){

                // fetch user from cache (easier)
                let user_client = client.users.cache.find(user => user.username === username);

                // start ping implementation
                if(command == "ping"){

                    let ping_no = new_argv[1];
                
                    
                    if(checkCommand("ping", new_argv)){

                        const channel = client.channels.cache.find(channel => channel.name === "general");
                        
                        var no_pings = 0; 
        
                        if(channel != null){
        
                            // ping user with 2 second delay
                            function pingUser(){
        
                                setTimeout(function(){
        
                                    no_pings = no_pings + 1;
        
                                    if(no_pings <= ping_no){
        
                                        channel.send(user_client.toString());
        
                                        pingUser();
                                    }
        
                                }, 5000)
                            }
                            pingUser();
                            // channel.send(theUser.toString());    
                        }
                        
                    }
                    //show correct usage
                    else{
                        message.reply(command_list.get('ping')[command_list.get('ping').length - 1]);
                    }

                } // end ping

                // start collect command

                if(command == "collect"){

                    // only let server administrators collect messages
                    if(message.member.permissions.has("ADMINISTRATOR")){
                        
                        let filter = m => m.author.username === user_client.username;
                        
                        let collector = new Discord.MessageCollector(message.channel, filter);
                        
                        collector.on('collect', (m) =>{

                            // add messages to database to be processed by show command

                            db.saveMessage(user_client.username, m.content, (result) => {

                                console.log(result);
                            })

                            if(m.content =="!stopc" && (message.author.id === m.author.id)){
                                collector.stop();
                            }
                    });
            
                    collector.on('end', collected => {
                        console.log("Collected " + collected);
                    });
                }
                }  // end collect

                // start show command
                if(command == "show"){

                    let user = new_argv[0];
                    let search_term = new_argv[1];

                    if(checkCommand("show", new_argv)){

                    db.getMessages(user, (messages) => {
                        console.log(messages);

                        // search messages containing term

                        var temp = [...messages];

                        let message_tok = tok(temp, [], 0, false);

                        temp = [...messages];
                        

                        if(search_term != null){

                            let indices = message_tok.map((e, i) => e === search_term ? i : '').filter(String);
                            let tok_indices = tok(temp, [], 0, true); // each token mapped to sentence


                            // find all messages containing occurrence of search term
                            let sentences = findSentence(indices, tok_indices);

                            //send the messages to the requester
                            if(sentences.length > 0){

                                message.author.send(`Here are all the messages from ${user} containing "${search_term}":`);

                                for(var i = 0; i < sentences.length; i++){
                                    message.author.send(sentences[i]);
                                }
                            }
                            else{
                                message.author.send(`No messages were found from ${user} containing ${search_term}`);
                            }   
                        }
                    })
                    }
                    else{
                        message.reply(command_list.get(command)[command_list.get(command).length - 1]);
                    }
                } // end show
            }
            // Can't find user
            else{
                if(command_list.get(command)[command_list.get(command).length - 2 ] != "nonuser"){

                    message.reply(command_list.get(command)[command_list.get(command).length - 1]);
                }
                
            }
        } // end getMembers async func
        
        getMembers();
    
    }
    
    if(message.content.startsWith(prefix) && command_list.has(command)){
        valUser();
    }

    // play video from youtbube
    // todo: implement a queuing option for playing songs
    if(command_list.has(command))
    {

        if(command == "play"){

            // console.log(findLink("alesso somebody to use"));

            // if user wants to play song w/o link
            if(args.length > 1 ){


                const search_string = args.join(" ");

                console.log(search_string);

                search(search_string, opts, function(err, results){

                    if(err){
                        return console.log(err);
                    }

                    const titles = results.map(video => video.title);
                    const links = results.map(video => video.link);

                    // check to see if any links contain (official audio) - best match
                    const hasAudio = hasoffcAudio(titles);

                    // take video from search results that most closely resembles the given title
                    var best_match = string_similarity.findBestMatch(search_string, titles).bestMatchIndex;
                    if(hasAudio != -1){
                        best_match = hasAudio;
                    }
                    const link = links[best_match];
                    const title = titles[best_match];
                    const currently_playing = [curr_song(args, title)];

                    play_song(link, currently_playing);
                })


            }

            else{

                const link = args[0];

                // attempt to find artist, song name from link 
                const getData = async () => {

                    const videoURL = link;
                    const requestURL = `https://youtube.com/oembed?url=${videoURL}&format=json`;    
                    const result = await axios.get(requestURL);

                    console.log(result.data.title);
                    let title = result.data.title.split(" ");
                    let delim = title.indexOf("-");

                    const artist = title.slice(0, delim).join(" ");
                    let song_name = title.splice(delim + 1).join(" ");

                    if(song_name.indexOf("(") != -1){
                        song_name = song_name.slice(0, song_name.indexOf("("));
                    }

                    // const link = args[0];
                    play_song(link, [artist, song_name, 0]);

                }

                getData();

            }
        }

        // show lyrics to song being played
        else if(command == "lyrics"){

            // lyrics have not yet been displayed
            if(song_queue.length > 0){

                let current_song = song_queue[0][0];
            
                if(current_song[2] == 0){
                    console.log(current_song[0] + " " + current_song[1]);

                    current_song[2] = 1;
                    const channel = client.channels.cache.find(channel => channel.name === "general");

                    (async function (artist, title){

                        console.log(artist + " " + title);
                        let lyrics = await lyrics_finder(artist, title) || "Not found";
                        let new_lyrics = " ";

                        // if lyrics length > 2000, send in segements of 2000
                        if(lyrics.length > 2000){

                            var i = 0;
                            while(i < lyrics.length){

                                if( (i + 1) % 2000 == 0){

                                    try {
                                        channel.send(new_lyrics);
                                        new_lyrics = " ";
                                    } catch (error) {  // ignore api error, where length > 2000 chars
                                        console.log(" ");
                                    }
                                }
                                else{
                                    new_lyrics += lyrics[i];
                                }
                                i++;
                            }
                            // send remaining lyrics (if any)
                            channel.send(new_lyrics);
                        }
                        else{
                            channel.send(lyrics);
                        }
                    })(current_song[0], current_song[1]);
                }
                else{
                    message.reply("Lyrics have already been displayed!");
                }
            }
            else{
                message.reply("No songs are currently playing!");
            }   
        }
        else if(command == "tweet"){

            if(checkCommand("tweet", args)){
                tweet(args[0], "x", message, 1);
            }
            else{
                message.reply(command_list.get(command)[command_list.get(command).length - 1]);
            }
        }
        else if(command  == "stoptweet"){

            const user = message.member.id;

            if(active_thread.get(user) != undefined){

                console.log(kill(active_thread.get(user)[1]));
                monitor_list.delete(active_thread.get(user)[0]);
                active_thread.delete(user);
            }
        }
        else if (command == "live"){

            const twitch_user = args[0]

            if(checkCommand("live", args)){

                console.log("yes");
                const islive = spawn('python', ['islive.py', twitch_user]);

                islive.stdout.on('data', (data) =>{

                    let live_channel = `${data}`;
                    const channel = client.channels.cache.find(channel => channel.name === "general");

                    channel.send(live_channel);

                })
            }   
        }
    }   

});
function hasoffcAudio(lst){

    for (var i = 0; i < lst.length; i++){

        let title = lst[i];
        if (title.indexOf("(") != -1){
            let temp = title.slice(title.indexOf("("));

            if(temp == "(Official Audio)"){
                return i; 
            }
        }
    }
    return -1;
}

// returns artist, song name from input parameter list
function curr_song(args, song_title){

    const delim = song_title.split(" ").indexOf("-");

    var artist = [];
    var title = [];

    let titles_tok = song_title.split(" ").map(element => {
        return element.toUpperCase();
    });

    // find position of token relative to delimiter (-), indicating separation between artist, song name
    for(var i = 0; i < args.length; i++){


        if(titles_tok.indexOf(args[i].toUpperCase()) < delim){
            artist.push(args[i]);
        }
        else{
            title.push(args[i]);
        }
    }

    console.log(`Artist ${artist.join(" ")}`);
    console.log(`Title ${title}`);

    return [artist.join(" "), title.join(" "), 0];

}
function play_song(link, elem){

        const stream = ytdl(link, {filter: 'audioonly', quality: 'highestaudio'}); // obtains streamable audio source
        const resource = createAudioResource(stream); // using audio source, create readable audio source for discord
        const channel = client.channels.cache.find(channel => channel.name === "General");
        const txt_channel = client.channels.cache.find(channel => channel.name === "general");
        const collector = new Discord.MessageCollector(txt_channel);

        const player = createAudioPlayer({
            behaviors: {
                noSubscriber: NoSubscriberBehavior.Pause,
            },
        });
        // create connection used for voice chat channel 
        const connection = joinVoiceChannel({
            channelId: channel.id,
            guildId: channel.guild.id,
            adapterCreator: channel.guild.voiceAdapterCreator,
        })
        connection.subscribe(player); // subscribe this connection to audio player
        player.play(resource);
        player.on('error', error => {
            console.error(error);
        });
        
        player.on(AudioPlayerStatus.Playing, () =>{
            console.log("Started to play audio.");
            song_queue.push(elem)
        });
        // stop playing when idle state is encountered
        player.on(AudioPlayerStatus.Idle, () =>{
            console.log("Audio player Idle, stopping audio.");
            player.stop();
            song_queue = [];
        })
        
        // use to collector to poll for incoming pause/resume requests
        collector.on('collect', (m) =>{
            if(m.content == "!stop"){
                console.log(`Request stop by ${m.author}`);
                player.stop();
                collector.stop();
                song_queue = [];
            }
            if(m.content == "!pause"){
                console.log(`Request pause by ${m.author}`);
                player.pause();
            }
            if(m.content == "!resume"){
                console.log(`Request resume by ${m.author}`);
                
            }
        })
        collector.on('end', () =>{
            console.log("Player exited");
        })
}


// takes indices array of search term and returns corresponding sentences 
function findSentence(indices, arr){

    const occurences = new Set();

    for(var i = 0; i < indices.length; i++){
        
        let cand = arr[indices[i]];
        var index = cand.split("").indexOf("-");
        let map_index = parseInt(cand[index + 1]);

        let sentence = search_map[map_index];

        if(sentence != undefined){
            occurences.add(sentence);
        }

    }

    return [...occurences];

}
// tokenize the messages used for search
// sample input: ["hello world goodbye world", "test"] -> ["hello", "world", "goodbye"] ... etc
// start tok
function tok(lst, temp, index, search){

    let size = lst.length;
    let index_one = lst[0];
    let lst_rest = lst.splice(lst.length - (lst.length - 1)); // equivalent to cdr(lst)

    if(size == 0 ){
        return temp;
    }

    // if element a multiple word string

    if(index_one.toString().split(" ").length > 1){

        let tokn = index_one.toString().split(" ");
        
        if(search){
            search_map[index] = index_one.toString();
        }
        for(let i = 0; i < tokn.length; i++){

            if(search){
                temp.push(tokn[i] + "-" + index);
                
                
            }
            else{
                temp.push(tokn[i]);
            }
        }

        return tok(lst_rest, temp, index + 1, search);

    }
    // index is just one word, add to return array
    else{

        temp.push(index_one);
        return tok(lst_rest, temp, index + 1, search);

    }

} // end tok

// calls 'app' python script to interact with Twitter API
function tweet(username, old_tweet, message, user_requested){
    
    if( (user_requested == 1 && monitor_list.has(username)) || (active_thread.has(message.member.id) && user_requested == 1)){
        message.reply(`${username} is already being monitored or you already have a request!`);
    }
    else if(user_requested == 0 && monitor_list.has(username)){
        tweet_sub(username, old_tweet, message, user_requested);   
    }
    else{
        monitor_list.add(username);
        tweet_sub(username, old_tweet, message, user_requested);
    }
    
}

function tweet_sub(username, old_tweet, message, user_requested){

    const verify = spawn('python', ['tapp.py', 'verify', username]);

    verify.stdout.on('data', (data) =>{

        if(`${data}` != "not found"){

            const monitor = spawn('python', ['tapp.py', "monitor", username, old_tweet]);
            let pid = monitor.pid;

            // if thread initialized by user request (first request to monitor), push active thread
            if(user_requested == 1){
                active_thread.set(message.member.id, [username, pid]);
            }
            
            // if thread initialized by method call (re-monitor)
            if(user_requested == 0){
                active_thread.set(message.member.id, [username, pid]);
            }
            // if we aren't already monitoring user, spawn new process
            monitor.stdout.on('data', (data) => {
                const txt_channel = client.channels.cache.find(channel => channel.name === "general");
                txt_channel.send(`New tweet from ${username} : ${data}`);
                tweet(username, "x", message, 0);
            })
            monitor.stderr.on('data', (data) => {
                console.error(`stderr:  ${data}`);
            })
            monitor.on('close', (code) => {})
        }
    })
}
// command handler - check to see whether command is being used correctly
function checkCommand(the_command, args){

    let format = command_list.get(the_command);

    // if number of args less than required
    if(((format.length - 1) != args.length && format.indexOf("nonuser") == -1) || args.length == 0 
    || (format.length - 1 ) == args.length && format.indexOf("nonuser") != -1 )
    {
        return false;
    }
    
    for(let i = 0; i < format.length - 1; i++){


        //check to see if command argument is int
        // if int, test arg from args for int

        // console.log(typeof format[i] + " " + typeof args[i]);
        if(format[i] != "nonuser"){
        
            if(typeof format[i] === typeof args[i]){

            }
            else{

                if(typeof format[i] === 'number'){

                    if(parseInt(args[i] === NaN)){
                        return false;
                    }
                    else{
                        return true;
                    }
                }
                return false;
            }
    }
    }
    return true;
}


// internal find user helper
function findUser(username){
    //console.log("the username " + username);

    // check cache for user first
    let user = client.users.cache.find(user => user.username === "Bill");
    // const Members = the_guild.members.fetch().then(members => console.log(members.map(member => member.displayName)));
    
    return user;
}



// database data layer
var db = {

    getMessages(user, callback){
        messagesdb.getMessages(user, (result) =>{
            callback(result);
        })
    },
    saveMessage(user, message, callback){
        messagesdb.storeMessage(user, message, (result) =>{
            callback(result);
        })
    }

}

// check if string is url - used for video
function isURL(string){
    let url;

    try{
        url = new URL(string);
    }catch(_){
        return false;
    }
    return true;
    }
