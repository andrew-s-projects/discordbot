const MongoClient = require('mongodb').MongoClient;

const uri = "mongodb+srv://dbot123:Hibob1245!@cluster0.tnokn.mongodb.net/botmessages?retryWrites=true&w=majority";
const mongodbclient = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

let db = null;


mongodbclient.connect((err, connection) => {
    if (err) throw err;
    console.log("Connected to Db");
    db = connection.db();
})
const dbIsReady = () => {
    return db != null;
};
const getDb = () => {
    if (!dbIsReady())
        throw Error("Error connecting to Db");
    return db;
}

const storeMessage = (username, message, callback) => {

    getDb().collection("users").findOne({username: username}).then(user => {

        // user already exists, only add message
        if(user && user.username == username){

            getDb().collection("users").updateOne(
                {"username": username},
                {$push : {"usermessages": message}}
            );
            

            callback(`Message: ${message} under: ${username}`);



        }

        // create new user, and add message
        else{

            let new_user = {"username": username}

            getDb().collection("users").insertOne(new_user, (err) =>{
                
                if(err){
                    callback("User create error");
                }
                else{
                    getDb().collection("users").updateOne({ "username": username },
                        { $set: { "usermessages": [] } },
                        {
                            upsert: false,
                            multi: true
                        })
                    getDb().collection("users").updateOne(
                        {"username": username},
                        {$push : {"usermessages" : message}}
                        );
                    callback(`user ${username} created`);
                }
            })
        }
    })

    callback("Success");

}

const getMessages = async (username, callback) => {

    let user_messages = [];

    await getDb().collection("users").find({"username": username}, {'fields' : {'username' : 0}}).forEach(function (result){
        result.usermessages.forEach(message =>{
            user_messages.push(message);
        });
    })
    callback(user_messages);


}

module.exports = {storeMessage, getMessages};