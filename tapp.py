# Python script used to interact with twitter API

# from contextlib import nullcontext
# from tkinter.tix import Tree
import const
import tweepy
# from tweepy import Stream
# # from tweepy.streaming import StreamingClient
import threading
import time
import sys

# thread used to continue monitor tweets of specified user
# will pull most recent tweet

auth = tweepy.OAuthHandler(const.CONSUMER_KEY, const.CONSUMER_SECRET)
auth.set_access_token(const.ACCESS_TOKEN, const.ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)

class TweetListener(tweepy.Stream):

    def __init__(self, consumer_key, consumer_secret, access_token, access_token_secret, **kwargs):
        super().__init__(consumer_key, consumer_secret, access_token, access_token_secret, **kwargs)
        self.max_tweets = 5
        self.tweet_count = 0

    def on_status(self, status):
        self.tweet_count += 1
        
        if (self.tweet_count == self.max_tweets):
            self.running = False
            return False
        else:
            # print(status.text)
            print(status.text)
            return True
    
    def on_error(self, status_code):
        if status_code == 420:
            return False

    def on_closed(self, response):
        print("I closed it")
                     
class monitorThread(threading.Thread):

    def __init__(self, threadId, twitter_user, old_tweet):

        threading.Thread.__init__(self)
        
        self.threadId = threadId
        self.twitter_user = twitter_user
        self.old_tweet = old_tweet

    def run(self):
        check_tweet(self.twitter_user, self.old_tweet)


def check_tweet(twitter_user, old_tweet):

    new_tweet = []

    while True:

        time.sleep(60)

        tweets = api.user_timeline(screen_name = twitter_user,
        count = 1,
        include_rts = False,
        exclude_replies = True
        )
        if(tweets[0].text != old_tweet):
            # print(old_tweet)
            new_tweet.append(tweets[0].text)
            break

    print(str(''.join(str(e) for e in new_tweet)))

def main():

    twitter_user = " "
    old_tweet = " "

    if(len(sys.argv) > 0):

        command = str(sys.argv[1])

        if command == "monitor":

            twitter_user = sys.argv[2]
            old_tweet = str(sys.argv[3])

            # if this is the first time monitoring user
            if old_tweet == "x":

                tweet = api.user_timeline(screen_name = twitter_user, count = 1, include_rts = False, exclude_replies = True)
                old_tweet = tweet[0].text

                child_thread = monitorThread(1, twitter_user, old_tweet)
                child_thread.start()
                child_thread.join()
                
        if command == "tweet":

            string = sys.argv[2]
            api.update_status(status= string)
            print("Successfully tweeted")
        
        if command == "verify":

            x = str(sys.argv[2])
            try:
                user = api.lookup_users(screen_name = x)
                print(user.id_str)

            except Exception:
                print("not found")

    else:
        print("Incorrect Command")

    # myStream = TweetListener(const.CONSUMER_KEY, const.CONSUMER_SECRET, const.ACCESS_TOKEN, const.ACCESS_TOKEN_SECRET)
    # myStream.filter(follow= [user_id])


if __name__ == "__main__":
    main()       
    # print("hello")