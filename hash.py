
import hashlib
import sys
import codecs
import binascii
from binascii import unhexlify

def main():

    if (len(sys.argv)) == 2:

        arg = sys.argv[1]
        the_string = arg.encode('utf-8')
        the_string_hex = binascii.hexlify(the_string).decode('utf-8')
        print(the_string_hex)
    
    if len(sys.argv) == 3:
        arg = sys.argv[1]
        test = unhexlify(arg)
        test2 = test.decode('utf-8')
        print(test2)

if __name__ == "__main__":
    main()