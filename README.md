# DISCORD BOT V1.0 #

A documentation of the all purpose discord bot built by Andrew Streng.

### Description ###

The discord bot is a node.js server that interacts with user commands on the discord application 
and carries out a variety of functions. The Twitter and Twitch APIs are used in conjunction with the discord API
to further this goal. The main implemntation characteristic of this project is that it uses at least 3 different APIs
as well as python scripting to enable the functionality of the commands listed below.  

### Technologies ###
#### The technologies used in this project and their respective roles are as follows: ####
* Python - used for commands involving the Twitter API (tweepy) as well as a script to make a request to the twitch API
* Javascript - the main language used to interact with the discord application directly
* MongoDB - database used to extend the functionality of the discord application
* Heroku - used to deploy the final application

### Use Cases ###
#### The functionality of the bot is divided into 5 commands that offer a new interactive experience for the user: ####
* Ping user - pings user to given number of times with a delay
* Play audio - enables users to play songs directly in voice chat
* Collect messages - a database which allows admins in server to monitor messages from a certain user (even deleted) 
* Show messages - uses this database and searches it to find all messages containing a certain term
* Tweet
    * Monitor Tweets from Twitter User - notifies users when a certain twitter user publishes a new tweet
    * Tweet Messages from Discord - publishes tweets that were considered funny by users in server

### Use Cases in Action ###

#### Ping User ####
![Ping](useims/ping.PNG)
* We can see now how the notifications show up on the target client.

![Ping2](useims/ping2.PNG)

#### Play Audio ####
* Please check out this video for this command demonstration: https://www.youtube.com/watch?v=cYCWQVD81e0

#### Collect Messages ####

![Collect](useims/collect.PNG)

* Here are the messages in the database

![Collect2](useims/collect2.PNG)

* And finally all mesages containing search term are sent as a direct message to requester.

![Collect3](useims/collect3.PNG)

#### Tweet Funny Messages ####

![tweet](useims/tweet.PNG)

* Here is the message taken from discord and tweeted to the Bot's twitter account.

![tweet2](useims/tweet2.PNG)


#### Monitor Tweets from Specified User

![tweet3](useims/tweet3.PNG)

* Here is an example of the bot notifying users in the server that the President has tweeted.

### How to Use the Bot ###

* Enter the following link into your browser: https://discord.com/api/oauth2/authorize?client_id=939523779510554654&permissions=8&scope=bot (need to have a discord account)

* The following option box should appear, simply select which server you want to add to bot to.

![addbot](useims/addbot.PNG)

* After the bot has been successfully added, it should appear on the user list on the right side of the application, commands can now be used.

![userlist](useims/userlist.PNG)
### Questions? ###

* Contact me at : strengswift01@gmail.com