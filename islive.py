import requests
import time
import sys

def main():
        
        channelName = sys.argv[1]

        while True:

            time.sleep(60)

            contents = requests.get('https://www.twitch.tv/' + channelName).content.decode('utf-8')

            if 'isLiveBroadcast' in contents:
                print(channelName + ' is live at https://www.twitch.tv/' + channelName)
                break
            

if __name__ == "__main__":
    main()   